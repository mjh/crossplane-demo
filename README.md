# Crossplane Demo

I wanted to test out Crossplane, so here we are.
This repository exists to support my [blog post on Crossplane](https://nalth.is/crossplane).

Generally speaking: 
- The `/terraform` directory can create a small k8s cluster on [DigitalOcean](https://m.do.co/c/b0719d610a69).
- The `/crossplane_install` directory will install the controllers for Crossplane.
- The `/crossplane_configure` needs your credentials as appropriate, but will configure specific `providerConfigs`.
- The `/crossplane` will install an [S3 Static Site bucket](https://docs.aws.amazon.com/AmazonS3/latest/userguide/WebsiteHosting.html) (though the name won't work without changing Line 9) and an [Azure Kubernetes Service](https://docs.microsoft.com/en-us/azure/aks/) cluster.

The `.gitlab-.ci.yml` has some automation that needs some other plumbing, which I discuss on my [automating Crossplane with GitLab](https://nalth.is/automating-crossplane-with-gitlab) post, so be sure to check there!