resource "digitalocean_kubernetes_cluster" "crossplane-demo" {
  name    = "crossplane-demo"
  region  = "nyc1"
  version = "1.23.9-do.0"

  node_pool {
    name       = "crossplane-autoscale-worker-pool"
    size       = "s-2vcpu-2gb"
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 3
  }
}